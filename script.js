'use strict';

const {GLib, Gio} = imports.gi;
const Signals = imports.signals;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const ByteArray = imports.byteArray;



var Script = class Script{



    constructor(script){
        this._script = script;
        this._cancellable = new Gio.Cancellable();
        let streams = this._getStreams(this._script, this._cancellable);
        this._stdoutStream = streams.stdoutStream;
        this._stdinStream = streams.stdinStream;
        this._proc = streams.proc;
        this._exit = false;
    }

    stop(){
        this._proc.force_exit();
    }

    async connect(callback){
        if (typeof callback === 'function'){
            while(!this._exit){
                await this.read_async((output, resolve) =>{
                    callback(output);
                    resolve();
                });
            }
        }else{
            throw("CALLBACK IS NOT A FUNCTION: " + callback);
        }
    }
    //returns the (output, resolve) pair
    read_async(callback){
        if (typeof callback === 'function'){
        return new Promise(function(resolve, reject){
            this.readOutput((output) => {
                    callback(output, resolve);
                })
            }.bind(this));
        }else{
            throw("CALLBACK IS NOT A FUNCTION: " + callback);
        }
    }
    
    //exectues the callback when done writing
    writeInput(input, callback) {
        return new Promise(function(resolve, reject){
            if (typeof input !== 'string' || !(input instanceof String)){
                input = String(input);
            }

            if(!input.endsWith("\n")){
                input += "\n";
            }
            this._stdinStream.write_bytes_async(
                new GLib.Bytes(input),
                GLib.PRIORITY_DEFAULT, null, (stdin, res) => {
                    try {
                        stdin.write_bytes_finish(res);
                        if (typeof callback === 'function'){
                            callback();
                            resolve();
                        }
                    } catch (e) {
                        logError(e);
                        reject();
                    }
                }
            );
        }.bind(this));
    }

    readOutput(callback) {
        if (typeof callback === 'function'){
            this._stdoutStream.read_line_async(GLib.PRIORITY_LOW, null, (stdout, res) => {
                try {
                    let line = stdout.read_line_finish_utf8(res)[0];
                    
                    if (line !== null) {
                        
                        callback(line);
                        
                    }
                } catch (e) {
                    logError(e);
                }
            });
        }else{
            throw("CALLBACK IS NOT A FUNCTION: " + callback);
        }
    }

    _getStreams(script, cancellable){
        let stdinStream = null;
        let stdoutStream = null;
        let proc = null;
        try {
            proc = Gio.Subprocess.new(
                ['sh', '-c', script],
                (Gio.SubprocessFlags.STDIN_PIPE |
                 Gio.SubprocessFlags.STDOUT_PIPE)
            );
        
            // Watch for the process to exit, like normal
            proc.wait_async(cancellable, (proc, res) => {
                try {
                    proc.wait_finish(res);
                    this._exit = true;
                } catch (e) {
                    this._exit = true;
                    logError(e);
                }
            });
        
            // Get the `stdin`and `stdout` pipes, wrapping `stdout` to make it easier to
            // read lines of text
            stdinStream = proc.get_stdin_pipe();
            stdoutStream = new Gio.DataInputStream({
                base_stream: proc.get_stdout_pipe(),
                close_base_stream: true
            });
        
        } catch (e) {
            logError(e);
        }
        return {proc: proc, stdinStream: stdinStream, stdoutStream: stdoutStream}
    }
}