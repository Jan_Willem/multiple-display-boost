# Multiple Display Boost

This extension fixes the annoying stuttering when a seconddary diplay is connected on a Nvidia Prime enabled laptop. It does so by setting the performance level of the Nvidia card to maximum when a seccondary display is detected.

For this extension to work the Nvidia drivers should be installed and working.
To test if your drivers are working run:

`$ nvidia-smi`
