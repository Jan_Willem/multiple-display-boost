'use strict';

const {GLib, Gio} = imports.gi;
const Signals = imports.signals;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const ByteArray = imports.byteArray;

const Script = Me.imports.script;



var Commands = class Commands{

    constructor(){

        let poll_frequency = 1;
        
        this._nvidiaInUseScript = new Script.Script(this._scriptBuilder("nvidia-smi -q | grep \"Display Mode\"", poll_frequency));
        this._getPowerModeScript = new Script.Script(this._scriptBuilder("nvidia-settings -q GpuPowerMizerMode | grep \"Attribute 'GPUPowerMizerMode\"", poll_frequency));
        this._isConnectedToACScript = new Script.Script(this._scriptBuilder("cat /sys/class/power_supply/AC/online", poll_frequency));
        this._getPStateScript = new Script.Script(this._scriptBuilder("nvidia-smi -q | grep \"Performance State\"", poll_frequency));
        this._nvidiaInUse = false;
        this._powerModus = -1;
        this._connectedToAC = false;
        this._pState = "?";


    
        this._nvidiaInUseScript.connect((output) =>{
            this._nvidiaInUse = output.includes("Enabled");
        });

        this._isConnectedToACScript.connect((output) =>{
            this._connectedToAC = output == 1;
        });

        this._getPowerModeScript.connect((output) => {
            let length = output.length;
            this._powerModus = parseInt(output.substring(length-2, length-1));
        });

        this._getPStateScript.connect((output) => {
            let lenght = output.length;
            this._pState = output.substring(lenght-3, lenght);
        });

        this._nvidiaInUseScript.writeInput("\n");
        this._getPowerModeScript.writeInput("\n");
        this._isConnectedToACScript.writeInput("\n");
        this._getPStateScript.writeInput("\n");

        let powerScript = 
        `
        #! /bin/bash
        while true; do
            read mode
            nvidia-settings -a GpuPowerMizerMode=$mode
            echo $mode
        done`;
        //            #
        this._setPowerModeScript = new Script.Script(powerScript);
    }

    _scriptBuilder(command, poll_frequency){
        let res = 
        `
        #! /bin/bash
        read start
        while true; do
            sleep `+poll_frequency+`
            echo $(`+command+`)
        done`;
        return res;
    }


    /**
     * Checks if Nvidia driver is loaded and running.
     * @return {Object}
     */
    isNvidiaInUse(){
       return this._nvidiaInUse;
    }

    isConnectedToAC(){
        return this._connectedToAC;
    }

    getPowerMode(){
        return this._powerModus;
    }

    getPState(){
        return this._pState;
    }

    async setPowerMode(powerMode, callback){
        this._setPowerModeScript.writeInput(powerMode, callback);
    }


    stop(){
        this._getPowerModeScript.stop();
        this._isConnectedToACScript.stop();
        this._nvidiaInUseScript.stop();
        this._setPowerModeScript.stop();
    }
    
}