/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const GETTEXT_DOMAIN = 'my-indicator-extension';

const { GObject, St, GLib, Gio} = imports.gi;


const Gettext = imports.gettext.domain(GETTEXT_DOMAIN);
const _ = Gettext.gettext;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const Commands = Me.imports.commands;

function getSettings () {
    let GioSSS = Gio.SettingsSchemaSource;
    let schemaSource = GioSSS.new_from_directory(
      Me.dir.get_child("schemas").get_path(),
      GioSSS.get_default(),
      false
    );
    let schemaObj = schemaSource.lookup(
      'org.gnome.shell.extensions.mdb', true);
    if (!schemaObj) {
      throw new Error('cannot find schemas');
    }
    return new Gio.Settings({ settings_schema : schemaObj });
}
  


const Indicator = GObject.registerClass(
class Indicator extends PanelMenu.Button {

    

    _init() {
        super._init(0.0, _('My Shiny Indicator'));

        let submenu = new PopupMenu.PopupMenuItem('');
        let box = new St.BoxLayout();
        box.set_vertical(true);


        this.add_child(new St.Icon({
            icon_name: 'face-smile-symbolic',
            style_class: 'system-status-icon',
        }));

        // let box = new St.BoxLayout();
        this._powerModeLabel = new St.Label({ text: ' Menu ',
			y_expand: true,});
        this._powerModeLabel.set_text("Power Mode: ?");

        this._activeCardLabel = new St.Label({ text: ' Menu ',
        y_expand: true,});
        this._activeCardLabel.set_text("Card Active: ?");

        this._isACLabel = new St.Label({ text: ' Menu ',
        y_expand: true,});
        this._isACLabel.set_text("AC: ?");

        this._pStateLabel = new St.Label({ text: ' Menu ',
        y_expand: true,});
        this._pStateLabel.set_text("pState: ?");


        // toplabel.track_hover = false;
        // box.add(toplabel);
        // box.set_hover(false);

        // submenu.connect('activate', () => {
        //     Main.notify(_('Whatʼs up, fol    ks?'));
        // });
        submenu.add_child(box);
        box.add_child(this._activeCardLabel);
        box.add_child(this._powerModeLabel);
        box.add_child(this._pStateLabel);
        box.add_child(this._isACLabel);
        submenu.track_hover = false;
        this.menu.addMenuItem(submenu);
        // this.menu.b(toplabel);
    }

    setPowerMode(mode){
        this._powerModeLabel.set_text(mode);
    }

    setActiveCard(active){
        this._activeCardLabel.set_text(active);
    }

    setAC(ac){
        this._isACLabel.set_text(ac);
    }

    setPState(pState){
        this._pStateLabel.set_text(pState);
    }
});

class Extension {
    constructor(uuid) {
        this._uuid = uuid;
        ExtensionUtils.initTranslations(GETTEXT_DOMAIN);
    }

    enable() {
        this._settings = getSettings();
        this._commands = new Commands.Commands();
        this._updateInterval = this._settings.get_int('update-interval');
        log("===========================================================================--------------------------");
        this._indicator = new Indicator();
        Main.panel.addToStatusArea(this._uuid, this._indicator);
        this._update();

        log("my boolean:" + this._settings.get_boolean('tray-icon'));
        // this._commands._testScript(null);
    }

    disable() {
        this._indicator.destroy();
        this._indicator = null;
        this._settings = null;
        this._commands.stop();
    }

    _update(){
        Promise.timeout = function(priority = GLib.PRIORITY_DEFAULT, interval = 3000) {
            return new Promise(resolve => GLib.timeout_add(priority, interval, () => {
                resolve();
            }));
        };

        async function startAsync(){
            try{
                while(true){
                    await Promise.timeout();
                    this._indicator.setPowerMode("Power Mode: " + this._commands.getPowerMode());
                    this._indicator.setActiveCard("Card Active: " + this._commands.isNvidiaInUse());
                    this._indicator.setAC("AC: " + this._commands.isConnectedToAC());
                    this._indicator.setPState("pState: " + this._commands.getPState());
                    
                    // this._commands.setPowerMode(3, () => {log("DONE SETTING")});

                }
            }catch(e){
            }
            
        }
        this.startAsync = startAsync.bind(this);
        this.startAsync();
    }

}

function init(meta) {
    return new Extension(meta.uuid);
}


